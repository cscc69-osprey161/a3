/* BEGIN A3 SETUP */
/* This file existed for A1 and A2, but has been completely replaced for A3.
 * We have kept the dumb versions of sys_read and sys_write to support early
 * testing, but they should be replaced with proper implementations that 
 * use your open file table to find the correct vnode given a file descriptor
 * number.  All the "dumb console I/O" code should be deleted.
 */

#include <types.h>
#include <kern/errno.h>
#include <lib.h>
#include <thread.h>
#include <current.h>
#include <syscall.h>
#include <vfs.h>
#include <vnode.h>
#include <uio.h>
#include <kern/fcntl.h>
#include <kern/unistd.h>
#include <kern/limits.h>
#include <kern/stat.h>
#include <copyinout.h>
#include <synch.h>
#include <file.h>
#include <kern/seek.h>

/* This special-case global variable for the console vnode should be deleted 
 * when you have a proper open file table implementation.
 */
struct vnode *cons_vnode=NULL; 

/* This function should be deleted, including the call in main.c, when you
 * have proper initialization of the first 3 file descriptors in your 
 * open file table implementation.
 * You may find it useful as an example of how to get a vnode for the 
 * console device.
 */
void dumb_consoleIO_bootstrap()
{
  int result;
  char path[5];

  /* The path passed to vfs_open must be mutable.
   * vfs_open may modify it.
   */

  strcpy(path, "con:");
  result = vfs_open(path, O_RDWR, 0, &cons_vnode);

  if (result) {
    /* Tough one... if there's no console, there's not
     * much point printing a warning...
     * but maybe the bootstrap was just called in the wrong place
     */
    kprintf("Warning: could not initialize console vnode\n");
    kprintf("User programs will not be able to read/write\n");
    cons_vnode = NULL;
  }
}

/*
 * mk_useruio
 * sets up the uio for a USERSPACE transfer. 
 */
static
void
mk_useruio(struct iovec *iov, struct uio *u, userptr_t buf, 
	   size_t len, off_t offset, enum uio_rw rw)
{

	iov->iov_ubase = buf;
	iov->iov_len = len;
	u->uio_iov = iov;
	u->uio_iovcnt = 1;
	u->uio_offset = offset;
	u->uio_resid = len;
	u->uio_segflg = UIO_USERSPACE;
	u->uio_rw = rw;
	u->uio_space = curthread->t_addrspace;
}

/*
 * sys_open
 * just copies in the filename, then passes work to file_open.
 * You have to write file_open.
 * 
 */
int
sys_open(userptr_t filename, int flags, int mode, int *retval)
{
	char *fname;
	int result;

	if ( (fname = (char *)kmalloc(__PATH_MAX)) == NULL) {
		return ENOMEM;
	}

	result = copyinstr(filename, fname, __PATH_MAX, NULL);
	if (result) {
		kfree(fname);
		return result;
	}

	result =  file_open(fname, flags, mode, retval);
	kfree(fname);
	return result;
}

/* 
 * sys_close
 * You have to write file_close.
 */
int
sys_close(int fd)
{
	return file_close(fd);
}

/* 
 * sys_dup2
 * 
 */
int
sys_dup2(int oldfd, int newfd, int *retval)
{
	int oldfd_result;
	struct opfile *of;

	//Check that both oldfd and newfd are valid entires in our file table
	if (oldfd < 0 || oldfd >= __OPEN_MAX || newfd < 0 || newfd >= __OPEN_MAX){
		return EBADF;
	}

	//Check that oldfd actually references an open file
	//If oldfd references an open file, we let *of be that file
	oldfd_result = filetable_find(oldfd, &of);
	if (oldfd_result){
		return oldfd_result;
	}

	//If newfd references an open file, we close it
	if (curthread->t_filetable->opfiles[newfd] != NULL){
		file_close(newfd);
	}

	//Copy the file descriptor oldfd onto newfd
	curthread->t_filetable->opfiles[newfd] = of;

	//Increment the refernce count of *of
	lock_acquire(of->of_lock);
	of->ref_count++;
	lock_release(of->of_lock);

	*retval = newfd;
	return 0;
}

/*
 * sys_read
 * calls VOP_READ.
 * 
 * A3: This is the "dumb" implementation of sys_write:
 * it only deals with file descriptors 1 and 2, and 
 * assumes they are permanently associated with the 
 * console vnode (which must have been previously initialized).
 *
 * In your implementation, you should use the file descriptor
 * to find a vnode from your file table, and then read from it.
 *
 * Note that any problems with the address supplied by the
 * user as "buf" will be handled by the VOP_READ / uio code
 * so you do not have to try to verify "buf" yourself.
 *
 * Most of this code should be replaced.
 */
int
sys_read(int fd, userptr_t buf, size_t size, int *retval)
{
	struct uio user_uio;
	struct iovec user_iov;
	int result;
	int offset = 0;
	struct opfile *of;

	//Verify that fd is a valid file descriptor	 
	if (fd < 0 || fd >= __OPEN_MAX) {
	  return EBADF;
	}

	//Find the opfile at fd for this thread
	result = filetable_find(fd, &of);
	if (result){
		return result;
	}

	lock_acquire(of->of_lock);
	//Get the offset for this particular opfile
	offset = of->of_offset;

	/* set up a uio with the buffer, its size, and the current offset */
	mk_useruio(&user_iov, &user_uio, buf, size, offset, UIO_READ);

	/* does the read */
	result = VOP_READ(of->of_vnode, &user_uio);
	if (result) {
		lock_release(of->of_lock);
		return result;
	}

	//Update the offset for this particular opfile after the read
	of->of_offset = of->of_offset + size;
	lock_release(of->of_lock);

	/*
	 * The amount read is the size of the buffer originally, minus
	 * how much is left in it.
	 */
	*retval = size - user_uio.uio_resid;

	return 0;
}

/*
 * sys_write
 * calls VOP_WRITE.
 *
 * A3: This is the "dumb" implementation of sys_write:
 * it only deals with file descriptors 1 and 2, and 
 * assumes they are permanently associated with the 
 * console vnode (which must have been previously initialized).
 *
 * In your implementation, you should use the file descriptor
 * to find a vnode from your file table, and then read from it.
 *
 * Note that any problems with the address supplied by the
 * user as "buf" will be handled by the VOP_READ / uio code
 * so you do not have to try to verify "buf" yourself.
 *
 * Most of this code should be replaced.
 */

int
sys_write(int fd, userptr_t buf, size_t len, int *retval) 
{
        struct uio user_uio;
        struct iovec user_iov;
        struct opfile *of;
        int result;
        int offset = 0;

		//Verify that fd is a valid file descriptor	
        if (fd < 0 || fd >= __OPEN_MAX) {
          return EBADF;
        }

        if (buf == NULL){
          return EFAULT;
        }

        result = filetable_find(fd, &of);
        if (result){
        	return result;
        }

        //Lock the file
        lock_acquire(of->of_lock);

        offset = of->of_offset;

        /* set up a uio with the buffer, its size, and the current offset */
        mk_useruio(&user_iov, &user_uio, buf, len, offset, UIO_WRITE);

        /* does the write */
        result = VOP_WRITE(of->of_vnode, &user_uio);
        if (result) {
        	lock_release(of->of_lock);
        	return result;
        }

        of->of_offset = len;
        lock_release(of->of_lock);

        /*
         * the amount written is the size of the buffer originally,
         * minus how much is left in it.
         */
        *retval = len - user_uio.uio_resid;

    	return 0;
}

/*
 * sys_lseek
 * 
 */
int
sys_lseek(int fd, off_t offset, int whence, off_t *retval)
{
	int result;
	struct opfile *of;
	off_t new_position;
	struct stat *stats;

	//Make sure fd is valid
	if (fd < 0 || fd >= __OPEN_MAX){
		return EBADF;
	}

	//Make sure whence is valid
	if (whence != SEEK_SET || whence != SEEK_CUR || whence != SEEK_END){
		return EINVAL;
	}

	//Get the file from fd
	result = filetable_find(fd, &of);
	if (result){
		return result;
	}

	if (curthread->t_filetable->opfiles[fd] == NULL){
		return ESPIPE;
	}

	//Lock the file
	lock_acquire(of->of_lock);

	if (whence == SEEK_SET){
		new_position = offset;
	}
	else if (whence == SEEK_CUR){
		new_position = of->of_offset + offset;
	}
	else if (whence == SEEK_END){
		result = VOP_STAT(of->of_vnode, stats); 
		if (result){
			lock_release(of->of_lock);
			return result;
		}
	}

	*retval = new_position;

	//If retval is negative return error
	if (*retval < 0){
		lock_release(of->of_lock);
		return EINVAL;
	}

	//Perform seek
	result = VOP_TRYSEEK(of->of_vnode, *retval);
	if (result){
		lock_release(of->of_lock);
		return result;
	}

	//Update offset
	of->of_offset = *retval;

	//Release lock
	lock_release(of->of_lock);

	return 0;
}


/* really not "file" calls, per se, but might as well put it here */

/*
 * sys_chdir
 * 
 */
int
sys_chdir(userptr_t path)
{
	char pathname[__PATH_MAX];
	int result;

	//Make sure that the path is valid
	if (pathname == NULL){
		return EFAULT;
	}

	//Copy the user supplied path variable into a kernel address
	result = copyinstr(path, pathname, __PATH_MAX, NULL);

	//Make sure we actually copied it over to kernel space
	if (result){
		return result;
	}

	//Make the directory change
	result = vfs_chdir(pathname);

	//Make sure change in directory was successful
	if (result){
		return result;
	}

	return 0;
}

/*
 * sys___getcwd
 * 
 */
int
sys___getcwd(userptr_t buf, size_t buflen, int *retval)
{
		struct iovec user_iovec;
        struct uio user_uio;
        int result;

        //Make sure that buf is valid
        if (buf == NULL){
        	return EFAULT;
        }

        //Setup the uio
        mk_useruio(&user_iovec, &user_uio, buf, buflen, 0, UIO_READ);

        //Get the cwd
        result = vfs_getcwd(&user_uio);

        //Make sure we actually got the cwd
        if (result){
        	return result;
        }

        //Set return value to length of data actually stored
        *retval = buflen - user_uio.uio_resid;

        return 0;
}

/*
 * sys_fstat
 */
int
sys_fstat(int fd, userptr_t statptr)
{
	struct opfile *of;
	int result;
	struct stat *statbuf;

	//Check that fd is valid
	if (fd < 0 || fd >= __OPEN_MAX){
		return EBADF;
	}

	//Get opfile from this fd
	result = filetable_find(fd, &of);
	if (result){
		return result;
	}

	//Malloc for statbuf
	statbuf = kmalloc(sizeof(struct stat));
	//Make sure we actually allocated memory
	if (statbuf == NULL){
		return ENOMEM;
	}

	lock_acquire(of->of_lock);
	//Get file info
	result = VOP_STAT(of->of_vnode, statbuf);
	//If VOP_STAT failed, make sure to release lock and free statbuf
	if (result){
		kfree(statbuf);
		lock_release(of->of_lock);
		return result;
	}

	result = copyout(statbuf, statptr, sizeof(struct stat));

	if (result){
		return result;
	}

	kfree(statbuf);
	lock_release(of->of_lock);
	return 0;
}

/*
 * sys_getdirentry
 */
int
sys_getdirentry(int fd, userptr_t buf, size_t buflen, int *retval)
{
	struct opfile *of;
	int result;
	struct uio user_uio;
	struct iovec user_iovec;

	//Make sure that fd is valid
	if (fd < 0 || fd >= __OPEN_MAX){
		return EBADF;
	}

	//Make sure that buf is valid
	if (buf == NULL){
		return EFAULT;
	}

	//Find the opfile from fd
	result = filetable_find(fd, &of);
	if (result){
		return result;
	}

	//Setup uio
	mk_useruio(&user_iovec, &user_uio, buf, buflen, of->of_offset, UIO_READ);

	//Lock the file
	lock_acquire(of->of_lock);

	//Get directory information
	result = VOP_GETDIRENTRY(of->of_vnode, &user_uio);
	if (result){
		return result;
	}

	//Set return value to length of the filename
	*retval = buflen - user_uio.uio_resid;

	//Update our offset
	of->of_offset = user_uio.uio_offset;

	//Release the lock
	lock_release(of->of_lock);

	return 0;
}

/* END A3 SETUP */




