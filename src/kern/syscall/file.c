/* BEGIN A3 SETUP */
/*
 * File handles and file tables.
 * New for ASST3
 */

#include <types.h>
#include <kern/errno.h>
#include <kern/limits.h>
#include <kern/stat.h>
#include <kern/unistd.h>
#include <kern/fcntl.h>
#include <file.h>
#include <syscall.h>
#include <vnode.h>
#include <synch.h>
#include <current.h>
#include <vfs.h>
#include <thread.h>

/*
 * Given the open file, free it by closing attached vnode and lock,
 * then kfree itself.
*/
static void opfile_close(struct opfile *of){
	lock_acquire(of->of_lock);
	if (of->ref_count > 1) {
		// Open file is referenced by more than just calling thread. 
		// Decrement ref count, as calling thread is letting go.
		of->ref_count--;
		lock_release(of->of_lock);
	}
	else {
		// Open file is only referenced by calling thread. Close it.
		KASSERT(of->ref_count == 1);
		lock_release(of->of_lock);
		lock_destroy(of->of_lock);
		vfs_close(of->of_vnode);
		kfree(of);
	}
}

/*** openfile functions ***/

/*
 * file_open
 * opens a file, places it in the filetable, sets RETFD to the file
 * descriptor. the pointer arguments must be kernel pointers.
 * NOTE -- the passed in filename must be a mutable string.
 * 
 * A3: As per the OS/161 man page for open(), you do not need 
 * to do anything with the "mode" argument.
 */
int
file_open(char *filename, int flags, int mode, int *retfd)
{
	// Allocate mem for open file.
	struct opfile *of = kmalloc(sizeof(struct opfile));
	if (!of){ // of not allocated.
		return ENOMEM;
	}
	// Open vnode for given filename. 
	int result = vfs_open(filename, flags, mode, &(of->of_vnode));
	if (result){ // vnode open failed.
		kfree(of);
		return result;
	}
	// Create a lock for open file.
	of->of_lock = lock_create("opfile open");
	if (!of->of_lock){ // Lock creation failed.
		vfs_close(of->of_vnode);
		kfree(of);
		return ENOMEM;
	}
	// Set int fields (flag, offset, and reference count)
	of->of_flags = flags;
	of->of_offset = 0;
	of->ref_count = 1;
	// Sequentially find an open slot in curthread's file table.
	// Insert open file in first open slot.
	int i = 0;
	for (i = 0; i < __OPEN_MAX; i++){
		if (!curthread->t_filetable->opfiles[i]){
			curthread->t_filetable->opfiles[i] = of;
			break; // Prevents i from reaching __OPEN_MAX on success.
		}
	}
	// curthread's file table is full.
	if (i == __OPEN_MAX){
		vfs_close(of->of_vnode);
		lock_destroy(of->of_lock);
		kfree(of);
		return EMFILE;
	}

	*retfd = i;
	return 0;
}


/* 
 * file_close
 * Called when a process closes a file descriptor.  Think about how you plan
 * to handle fork, and what (if anything) is shared between parent/child after
 * fork.  Your design decisions will affect what you should do for close.
 */
int
file_close(int fd)
{
	// Check if fd is valid.
	if (fd < 0 || fd >= __OPEN_MAX){
		return EBADF;
	}
	// Get open file at fd.
	struct opfile *of = curthread->t_filetable->opfiles[fd];
	// Check if fd is indexing a null entry.
	if (!of){
		return EBADF;
	}
	// fd, and therefore of, is a valid open file.
	opfile_close(of);

	// Set entry to null.
	curthread->t_filetable->opfiles[fd] = NULL;

	return 0;
}

/*** filetable functions ***/

/* 
 * filetable_init
 * pretty straightforward -- allocate the space, set up 
 * first 3 file descriptors for stdin, stdout and stderr,
 * and initialize all other entries to NULL.
 * 
 * Should set curthread->t_filetable to point to the
 * newly-initialized filetable.
 * 
 * Should return non-zero error code on failure.  Currently
 * does nothing but returns success so that loading a user
 * program will succeed even if you haven't written the
 * filetable initialization yet.
 */

int
filetable_init(void)
{
	// Allocate memory for per-process file table.
	curthread->t_filetable = kmalloc(sizeof(struct filetable));
	if (!curthread->t_filetable){ // No memory
		return ENOMEM;
	}
	// Initialize all entries to NULL.
	for (int i = 0; i < __OPEN_MAX; i++){
		curthread->t_filetable->opfiles[i] = NULL;
	}

	// file_open calls methods that modifies the filename that is passed in.
	// Create three separate Strings. 
	char std_in[8];
	char std_out[8];
	char std_err[8];
	int result;
	int *garbage;

	// 'con:' is the file descriptor attached to the console device
	strcpy(std_in, "con:");
	strcpy(std_out, "con:");
	strcpy(std_err, "con:");

	// file_open initializes new open files sequentially from index 0. 
	result = file_open(std_in, O_RDONLY, 0, garbage);
	KASSERT(*garbage == 0);
	if (result){
		return result;
	}

	result = file_open(std_out, O_WRONLY, 0, garbage);
	KASSERT(*garbage == 1);
	if (result){
		return result;
	}

	result = file_open(std_err, O_WRONLY, 0, garbage);
	KASSERT(*garbage == 2);
	if (result){
		return result;
	}

	return 0;
}

/*
 * filetable_destroy
 * closes the files in the file table, frees the table.
 * This should be called as part of cleaning up a process (after kill
 * or exit).
 */
void
filetable_destroy(struct filetable *ft)
{
	for (int i = 0; i < __OPEN_MAX; i++){
		if (ft->opfiles[i]){
			opfile_close(ft->opfiles[i]);
		}
	}
	kfree(ft);
}


/* 
 * You should add additional filetable utility functions here as needed
 * to support the system calls.  For example, given a file descriptor
 * you will want some sort of lookup function that will check if the fd is 
 * valid and return the associated vnode (and possibly other information like
 * the current file position) associated with that open file.
 */

/*
 * Copy current thread's file table to given thread.
 * 
 */
int
filetable_copy_to_thread(struct thread *t)
{
	KASSERT(t != NULL); // given thread cannot be null pointer.

	// Allocate memory for copied table.
	t->t_filetable = kmalloc(sizeof(struct filetable));
	if (!t->t_filetable){ // No memory.
		return ENOMEM;
	}
	// Copy curthread file table to destination thread. 
	struct opfile *curOf;
	for (int i = 0; i < __OPEN_MAX; i++) {
		// Save open file with fd 'i' of current thread for easy access.
		curOf = curthread->t_filetable->opfiles[i];
		if (curthread->t_filetable->opfiles[i]) {
			// Is a valid file.
			lock_acquire(curOf->of_lock);
			curOf->ref_count++; // Increment ref count on copying.
			lock_release(curOf->of_lock);
			t->t_filetable->opfiles[i] = curOf;
		}
		else {
			// Simply set as NULL.
			t->t_filetable->opfiles[i] = NULL;
		}
	}

	return 0;
}

/*
 *Verify if fd is valid and if fd references an open file
 */
int
filetable_find(int fd, struct opfile **of){
	if (fd < 0 || fd >= __OPEN_MAX){
		return EBADF;
	}

	if(curthread->t_filetable->opfiles[fd] == NULL){
		return EBADF;
	}

	*of = curthread->t_filetable->opfiles[fd];
	return 0;
}

/* END A3 SETUP */
